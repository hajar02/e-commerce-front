import axios from "axios";
import { Article, Stock } from "./entities";

export async function fetchAllStock(){
    const response = await axios.get<Stock[]>('/api/stock');
    return response.data;
}


export async function fetchAllSizeFromOneArticle(id:number) {
    const response = await axios.get<Article[]>('/api/stock/'+id+'/article');
    return response.data;
  }

  export async function fetchOneArticleStock(id:number){
    const response = await axios.get<Stock[]>('/api/article/'+id+'/stock');
    return response.data;
}


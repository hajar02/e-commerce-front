import axios from "axios";
import { LineArticles, Order, Stock } from "./entities";

export async function fetchCurrentOrder(){
    const response = await axios.get<Order>('/api/order/current');
    return response.data;
}

export async function updateLineArticle(lineArticle:LineArticles){
    const response = await axios.put<LineArticles>('/api/lineArticle/'+lineArticle.id, lineArticle);
    return response.data;
}


export async function deleteLineArticle(lineArticle:LineArticles) {
    const response = await axios.delete<LineArticles>('/api/lineArticle/'+lineArticle.id)
    return response.data;
}

export async function postNewOrder(order:Order){
    const response = await axios.post<Order>('/api/order', order);
    return response.data;
}

export async function updateOrder(order:Order){
    const response = await axios.patch<Order>('/api/order/'+order.id, order);
    return response.data;
}
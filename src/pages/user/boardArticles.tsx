import Table from 'react-bootstrap/Table';
import AddOneArticle from '@/components/AddOneArticle';
import LineArticleTable from '@/components/LineArticleTable';

export default function board() {

  console.log('boh')

  return (
    <>
      <section className='commandes'>
        <h2>BOARD</h2>
        <Table striped bordered hover size="sm" className='tablecommandes'>
          <thead>
            <tr>
              <th>Image</th>
              <th>N°</th>
              <th>Titre</th>
              <th>Artiste</th>
              <th>Description</th>
            </tr>
          </thead>
          <tbody>
            <LineArticleTable/>
          </tbody>
        </Table>
        <AddOneArticle />
      </section>


    </>
  );
}
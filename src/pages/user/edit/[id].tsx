import { updateArticle, fetchOneArticle } from "@/article-service";
import { Article } from "@/entities";
import { useRouter } from "next/router";
import { useState, useEffect } from "react";
import Form from "@/components/Form";


export default function EditArticle() {
    const router = useRouter();
    const [article, setArticle] = useState<Article>();
    const {id} = router.query;

    useEffect(() => {
        if (!id) {
            return;
        }
        fetchOneArticle(Number(id))
            .then(data => setArticle(data))
            .catch(error => {
                console.log(error);
                if (error.response.status == 404) {
                    router.push('/404');
                }
            });

    }, [id]);

    async function update(article: Article) {
        const updated = await updateArticle(article);
        setArticle(updated);
    }

    return (
        <>
            <h1>Edit Article</h1>
            <Form edited={article} onSubmit={update} />
        </>
    );
}
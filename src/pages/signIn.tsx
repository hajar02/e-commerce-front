import React, { useContext, useState } from 'react';
import { useForm } from "react-hook-form";
import { Button, Row, Col, Form } from 'react-bootstrap';
import { useRouter } from 'next/router';
import { AuthContext } from '@/auth/auth-context';
import { login, postUser } from "@/auth/auth-service";
import { Order, User } from '@/entities';
import { postNewOrder } from '@/order-service';


export default function SignIn() {

    const router = useRouter();
    const { setToken } = useContext(AuthContext);


    const [error, setError] = useState('');
    const { register, handleSubmit, formState: { errors } } = useForm<User>();

    const onSubmit = async (data: User) => {
        try {
            console.log(data);
            const sign = await postUser(data);

            const token = await login(String(data.email), String(data.hashedPassword));
            setToken(token);

            const newDate = new Date().toISOString().split('T')[0];

            const newOrder: Order = ({
                date: newDate,
                total: 0,
                deliveryTime: "5 à 10 jours",
                status: "en cours"
            });

            await postNewOrder(newOrder);

            router.push('/newAccount');
        } catch (error: any) {
            if (error.response?.status == 401) {
                setError('Invalid form completion');

            } else {
                setError('Server error');
            }
        }

    };




    return (
        <>
            <div className="mt-5" >
                <Row>
                    <Col className='col-md-6 offset-md-3 col-sm-8 offset-md-2 col-10 offset-1' >
                        <Form onSubmit={handleSubmit(onSubmit)}>

                            <Form.Group className="mb-3" controlId="formBasicFirstName">
                                <Form.Label>Firstname</Form.Label>
                                <Form.Control placeholder="Marie" type="text" defaultValue="" {...register("firstName", { required: true })} />
                                {errors.firstName && <Form.Text className="text-muted">This field is required</Form.Text>}
                            </Form.Group>

                            <Form.Group className="mb-3" controlId="formBasicLasttName">
                                <Form.Label>Lastname</Form.Label>
                                <Form.Control placeholder="Leblanc" type="text" defaultValue="" {...register("lastName", { required: true })} />
                                {errors.lastName && <Form.Text className="text-muted">This field is required</Form.Text>}
                            </Form.Group>

                            <Form.Group className="mb-3" controlId="formBasicEmail">
                                <Form.Label>Email address</Form.Label>
                                <Form.Control placeholder="marie.leblanc@outlook.com" type="email" {...register("email", { required: true })} />
                                {errors.email && <Form.Text className="text-muted">This field is required</Form.Text>}
                            </Form.Group>

                            <Form.Group className="mb-3" controlId="formBasicPassword">
                                <Form.Label>Password</Form.Label>
                                <Form.Control type="password" placeholder="lgH84hj35ù%" {...register("hashedPassword", { required: true })} />
                                {errors.hashedPassword && <Form.Text className="text-muted">This field is required</Form.Text>}
                            </Form.Group>

                            <Form.Group className="mb-3" controlId="formBasicPhoneNumber">
                                <Form.Label>Phone number</Form.Label>
                                <Form.Control type="text" placeholder="0612345678" defaultValue="" {...register("phoneNumber", { required: true, maxLength: 10 })} />
                                {errors.phoneNumber && <Form.Text className="text-muted">This field is required</Form.Text>}
                            </Form.Group>

                            <Form.Group className="mb-3" controlId="formBasicstreet">
                                <Form.Label>Street</Form.Label>
                                <Form.Control type="text" placeholder="1 rue des Lilas" defaultValue="" {...register("street", { required: true })} />
                                {errors.street && <Form.Text className="text-muted">This field is required</Form.Text>}
                            </Form.Group>

                            <Form.Group className="mb-3" controlId="formBasicZIPCode">
                                <Form.Label>ZIPCode</Form.Label>
                                <Form.Control type="text" placeholder="69000" defaultValue="" {...register("ZIPCode", { required: true, maxLength: 5 })} />
                                {errors.ZIPCode && <Form.Text className="text-muted">This field is required</Form.Text>}
                            </Form.Group>

                            <Form.Group className="mb-3" controlId="formBasicCity">
                                <Form.Label>City</Form.Label>
                                <Form.Control type="text" placeholder="Lyon" defaultValue="" {...register("city", { required: true })} />
                                {errors.city && <Form.Text className="text-muted">This field is required</Form.Text>}
                            </Form.Group>

                            <Button variant="primary" type="submit">
                                Submit
                            </Button>
                        </Form>
                    </Col>
                </Row>
            </div>
        </>
    )
}
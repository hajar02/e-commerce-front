import { AuthContext } from "@/auth/auth-context";
import ArticleCommande from "@/components/ArticleCommande";
import DetailCommande from "@/components/DetailCommande";
import ResumeCommande from "@/components/ResumeCommande";
import { Order } from "@/entities";
import { fetchCurrentOrder } from "@/order-service";
import Link from "next/link";
import { useContext, useEffect, useState } from "react";



export default function Panier() {


    const [order, setOrder] = useState<Order>();
    const { token } = useContext(AuthContext);

    useEffect(() => {
        fetchCurrentOrder()
            .then(data => setOrder(data))
            .catch(error => console.log(error))
    }, [])

    function Update() {
        fetchCurrentOrder()
            .then(data => setOrder(data))
            .catch(error => console.log(error))
    }

    return (
        <>
            <section className="panier">

                {order && token &&

                    <div className="container-fluid p-md-5 p-sm-3 p-1">
                        <h2 className="text-center">Mon panier</h2>
                        <div className="row full-width" >
                            <div className="col-md-6">

                                <ArticleCommande order={order} onQuantityChange={Update} />

                            </div>
                            <div className="col-md-6">

                                <ResumeCommande order={order} />

                                <DetailCommande order={order} />

                            </div>
                        </div>
                    </div>

                }

                {!token &&
                    <>
                        <div className="container-fluid p-md-5 p-sm-3 p-1">
                            <h2 className="text-center">Veuillez vous <Link href="/login">Connecter</Link></h2>

                        </div>
                    </>
                }

            </section>
        </>
    )

}
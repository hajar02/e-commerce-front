import type { AppProps } from 'next/app';
import 'bootstrap/dist/css/bootstrap.min.css';
import '@/styles/globals.css';
import axios from 'axios';
import { useEffect } from 'react';
import MainNavigation from '@/components/MainNavigation';
import Footer from '@/components/Footer';
import { AuthContextProvider } from '@/auth/auth-context';
import '../auth/axios-config';
import Header from '@/components/Header';


axios.defaults.baseURL = process.env.NEXT_PUBLIC_SERVER_URL;


export default function App({ Component, pageProps }: AppProps) {

  useEffect(() => {
    require("bootstrap/dist/js/bootstrap.bundle.min.js");
    document.title = "mettre le titre de notre site ici";
  }, []);

  return (
    <>
      <AuthContextProvider>
        <MainNavigation />
        <Header/>
        <Component {...pageProps} />
        <Footer />
      </AuthContextProvider>
    </>
  )
}

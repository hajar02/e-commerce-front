import React, { useContext, useState } from 'react';
import { useForm } from "react-hook-form";
import { Button, Row, Col, Form } from 'react-bootstrap';
import { useRouter } from 'next/router';
import { AuthContext } from '@/auth/auth-context';
import { login } from "@/auth/auth-service";
import { User } from '@/entities';


export default function LogIn() {

    const router = useRouter();
    const { setToken } = useContext(AuthContext);

    const [error, setError] = useState('');
    const { register, handleSubmit, formState: { errors } } = useForm<User>();

    const onSubmit = async (data: User) => {
        try {
            const token = await login(String(data.email), String(data.hashedPassword));
            setToken(token);
            alert('login successfull')
            router.back()
        } catch (error: any) {
            if (error.response?.status == 401) {
                setError('Invalid login/password');

            } else {
                setError('Server error');
            }
        }
    };


    return (
        <>
            <div className="mt-5" >
                <Row>
                    <Col xs={{ span: 6, offset: 3 }}>
                        <Form onSubmit={handleSubmit(onSubmit)}>

                            <Form.Group className="mb-3" controlId="formBasicEmail">
                                <Form.Label>Email address</Form.Label>
                                <Form.Control type="email" defaultValue="test@test.com" {...register("email")} />
                            </Form.Group>

                            <Form.Group className="mb-3" controlId="formBasicPassword">
                                <Form.Label>Password</Form.Label>
                                <Form.Control type="password" defaultValue="1234" {...register("hashedPassword")} />
                            </Form.Group>

                            <Button variant="primary" type="submit">
                                Submit
                            </Button>
                        </Form>
                    </Col>
                </Row>
            </div>
        </>
    )
}
import { User } from "@/entities"
import { fetchCurrentUser } from "@/user-service"
import { useState, useEffect } from "react"
import { Button } from "react-bootstrap"


export default function achat(){

    const [user, setUser] = useState<User>()

    useEffect(() => {
        fetchCurrentUser()
            .then(data => setUser(data))
            .catch(error => console.log(error))

    }, [])


    return(
        <>
        <div style={{height:"40vh"}} className="d-flex justify-content-center align-items-center flex-column">
            <h2>Merci : <span>{user?.firstName}</span> pour votre achat !</h2>
            <Button href="/">Accueil</Button>
        </div>
        </>
    )
}
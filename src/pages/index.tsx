import Nouveautes from "@/components/Nouveautes";
import Header from "@/components/Header";
import NosProduits from "@/components/NosProduits";
import QuiSommeNous from "@/components/QuiSommeNous";



export default function index() {

  return(
    <>
    <Nouveautes/>
    <NosProduits/>
    <QuiSommeNous/>
    </>
  )
}


import Commentaire from '@/components/Commentaire';
import { FaHeart } from 'react-icons/fa';
import { FaStar } from 'react-icons/fa';
import { useRouter } from "next/router";
import { useContext, useEffect, useState } from "react";
import { commentsByArticle, fetchOneArticle } from '@/article-service';
import { Article, Stock, LineArticles, Comment } from "@/entities";
import { fetchOneArticleStock } from '@/stock-service';
import { fetchCurrentOrder } from '@/order-service';
import { postLineArticle } from '@/lineArticle-service';
import { Dropdown } from 'react-bootstrap';
import { AuthContext } from '@/auth/auth-context';



export default function OneArticle() {
    
    const { token } = useContext(AuthContext);
    const router = useRouter();
    const { id } = router.query;
    const [article, setArticle] = useState<Article>();
    const [stocks, setStocks] = useState<Stock[]>();
    const [stock, setStock] = useState<Stock>();
    const [comment, setComment] = useState<Comment[]>();


    useEffect(() => {
        if (!id) {
            return;
        }
        commentsByArticle(Number(id))
            .then(data => setComment(data))
        fetchOneArticle(Number(id))
            .then(data => setArticle(data))
        fetchOneArticleStock(Number(id))
            .then(data => setStocks(data))
            .catch(error => {
                console.log(error);
                if (error.response.status == 404) {
                    router.push('/404');
                }
            });

    }, [id]);


    if (!article) {
        return <p>Loading...</p>
    }

    async function addToBasket() {

        try {const currentOrder = await fetchCurrentOrder()
        let id = 1
        if (article) {
            id = Number(article.id)
        }

        const lineArticle: LineArticles = ({
            articleId: id,
            quantity: 1,
            star: 5
        })


        if (stock) {
            console.log("current Order :"+currentOrder);
            await postLineArticle(lineArticle, currentOrder, stock);
            alert("Votre article a bien été ajouté au panier");

        } else {
            alert("Select a size");

        }} catch {
            alert(<a>You must sign in to buy</a>)
        }


    }

    console.log("stocks : "+{stocks})
    console.log("stock : "+{stock})

    return (
        <>

            <section className="onearticle">
                <div>
                    <img src={article.image} alt="" />
                </div>
                <div className="textarticle">
                    <h2>{article.name}</h2>

                    <p>Lorem ipsum dolor sit boh</p>
                    <hr />
                    <p>{article.description}</p>

                    <p className='price'>{stock && stock.price}{!stock && stocks && stocks[0].price} €</p>

                    <Dropdown className='pb-2'>
                        <Dropdown.Toggle variant="success" id="dropdown-basic">
                            {stock ?  "Size : "+stock.size : "Select a size"}
                        </Dropdown.Toggle>

                        <Dropdown.Menu>
                        {stocks?.map((item) =>
                            <Dropdown.Item key={item.id} as="button" onClick={() => setStock(item)} value={item.size}>{item.size}</Dropdown.Item>
                        )}
                        </Dropdown.Menu>
                    </Dropdown>
                                       



                    <div className='comments'>
                        <div className='stars'>
                            <FaStar className='star' /><FaStar className='star' /><FaStar className='star' /><FaStar className='star' /><FaStar className='star' />
                        </div>
                        <div className='reviews'> {comment ? comment.length : 0} reviews </div>
                    </div>

                    <div className="buttonsonearticle">
                        {!token && <button onClick={() => router.push('/signIn')}>Sign in to Buy</button>}{token && <button onClick={() => addToBasket()}>Add to basket</button>}
                        <button><FaHeart /></button>
                    </div>
                </div>
            </section>

            <section className='sectioncomments'>

                <h3>COMMENTS</h3>
                <hr />
                <button>Add comment</button>

<Commentaire/>

            </section>



        </>
    )
}

function handleChange(event: Event | undefined) {
    throw new Error('Function not implemented.');
}

import { fetchAllArticle, fetchAllArticleByArtist } from "@/article-service";
import ArticleCard from "@/components/ArticleCard";
import Header from "@/components/Header";
import { Article } from "@/entities";
import { GetServerSideProps } from "next";
import Link from "next/link";
import { useState, useEffect } from "react";
import { Button, Nav } from "react-bootstrap";

// interface Props {
//   articles: Article[];
// }


// export default function produits({ articles }: Props) {

export default function produits() {

  const [articles, setArticles] = useState<Article[]>([]);
  const [activeArtist, setActiveArtist] = useState("");

  useEffect(() => {
    fetchAllArticle()
      .then(data => setArticles(data))
  }, []);

  function articleArtist(artist: string) {
    if (artist === activeArtist) {
      setActiveArtist("");
      fetchAllArticle()
        .then((data) => setArticles(data));
    } else {
      setActiveArtist(artist);
      fetchAllArticleByArtist(artist)
        .then((data) => setArticles(data));
    }

  }

  return (
    <>
      <section className="container-fluid">
        <div className="d-flex flex-column align-items-center pt-3 pt-md-5">

          <h2 className="pt-3 pt-md-5">NOS PRODUITS</h2>
          <hr className="little-hr" />

          <div>
            <Nav className="nav-artists">
              <Nav.Link active={activeArtist === "Alexandre"} onClick={() => articleArtist("Alexandre")}>ALEXANDRE</Nav.Link>
              <p className="py-1">|</p>
              <Nav.Link active={activeArtist === "Hajar"} onClick={() => articleArtist("Hajar")}>HAJAR</Nav.Link>
              <p className="py-1">|</p>
              <Nav.Link active={activeArtist === "Juliette"} onClick={() => articleArtist("Juliette")}>JULIETTE</Nav.Link>
            </Nav>
          </div>

        </div>

        <div className="p-1 p-sm-3 p-md-5 mx-1 mx-sm-3 mx-md-5">
          <div className="p-1 p-sm-3 p-md-5 mx-1 mx-sm-3 mx-md-5">
            <div className="row g-2 g-sm-3 g-md-5">
              {articles.map(item =>

                <div key={item.id} className="col-sm-6 col-md-4 col-lg-3">

                  <ArticleCard article={item} />

                </div>)}

            </div>
          </div>
        </div>
      </section>

    </>
  )
}

// export const getServerSideProps: GetServerSideProps<Props> = async () => {

//   return {
//     props: {
//       articles: await fetchAllArticle()

//     }
//   }
// }

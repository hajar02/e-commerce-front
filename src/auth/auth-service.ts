import { User } from "@/entities";
import axios from "axios";



export async function login(email:string,hashedPassword:string) {
    const response = await axios.post<{token:string}>('/api/login', {email, hashedPassword});
    return response.data.token;
}

export async function fetchUser() {
    const repsonse = await axios.get<User>('/api/account');
    return repsonse.data;
}

export async function postUser(user:User){
    const response = await axios.post<User>('/api/user', user);
    return response.data;
}
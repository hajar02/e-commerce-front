
import { FaStar } from 'react-icons/fa';
import { Comment } from "@/entities";
import { useRouter } from 'next/router';
import { useEffect, useState } from 'react';
import { commentsByArticle } from '@/article-service';

export default function Commentaire() {

    const router = useRouter();
    const { id } = router.query;
    const [comment, setComment] = useState<Comment[]>();


    useEffect(() => {
        commentsByArticle(Number(id))
            .then(data => setComment(data))
            .catch(error => {
                console.log(error.response);
                if (error.response.status == 404) {
                    router.push('/404');
                }
            });

    }, []);

    return (
        <>
        {console.log(comment)}
            {comment && comment.map((item) => 
                <div className="comment" key={item.id}>
                    <h4>User</h4>
                    {/*<div className='stars'>
                        <FaStar className='star' /><FaStar className='star' /><FaStar className='star' /><FaStar className='star' /><FaStar className='star' />
                    </div>*/}
                    <p>{item.text}</p>
                </div>
            )}

        </>
    )

}      
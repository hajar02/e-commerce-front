import { AuthContext } from "@/auth/auth-context";
import { Order } from "@/entities";
import { fetchCurrentOrder, postNewOrder, updateLineArticle, updateOrder } from "@/order-service";
import { useContext, useEffect, useState } from "react"
import { Button } from "react-bootstrap";


interface Props {
    order: Order
}

export default function ResumeCommande({ order }: Props) {

    function totalQuantity() {
        let totalQuantity = 0
        if (order.lineArticles) {
            for (let index = 0; index < order.lineArticles.length; index++) {
                totalQuantity = totalQuantity + order.lineArticles[index].quantity
            }
        }
        return totalQuantity
    }

    function totalPrice(): number {
        let totalPrice = 0
        if (order.lineArticles) {
            for (let index = 0; index < order.lineArticles.length; index++) {
                totalPrice += order.lineArticles[index].productPrice * order.lineArticles[index].quantity
            }
        }

        return Number(totalPrice.toFixed(2))
    }

    async function onBuy() {
        const newDate = new Date().toISOString().split('T')[0];
        order.status = "en traitement";
        order.total = totalPrice();
        order.lineArticles = undefined;
        order.date = newDate;
        await updateOrder(order);
        const newOrder: Order = ({
            date: newDate,
            total: 0,
            deliveryTime: "5 à 10 jours",
            status: "en cours",
            user_id: order.user_id
        });
        await postNewOrder(newOrder);
        console.log(newOrder);
    }

    return (
        <>
            <>
                <>
                    <div className="col-md-12 d-flex justify-content-center mt-5 mb-5">
                        <div className="card mb-3" style={{ width: "630px" }}>
                            <div className="row g-0 bgcolor">

                                <div className="col-md-8">
                                    <div className="card-body mt-1">
                                        <h5 className="card-title">Resume Commande</h5>
                                        <hr />
                                        <div className=" mt-3 mb-3">

                                            <p>Nombre d'articles : <span>{totalQuantity()}</span></p>


                                        </div>
                                        <p>total : {totalPrice()} $</p>
                                        <Button href="/achat" onClick={() => onBuy()} className="mt-3">Buy</Button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </>
            </>
        </>
    )
}
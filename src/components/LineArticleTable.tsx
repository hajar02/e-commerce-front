import { Article } from "@/entities";
import { useRouter } from 'next/router';
import { useEffect, useState } from 'react';
import { deleteArticle, fetchAllArticle } from '@/article-service';
import { FaTrashAlt, FaEdit } from 'react-icons/fa';
import Link from "next/link";

export default function LineArticleTable() {

    const router = useRouter();
    /*const { id } = router.query;*/
    const [articles, setArticles] = useState<Article[]>();
    /*const [oneArticle, setOneArticle] = useState<Article>();*/

    useEffect(() => {
        fetchAllArticle()
            .then(data => setArticles(data))
            .catch(error => {
                console.log(error);
                if (error.response.status == 404) {
                    router.push('/404');
                }
            });
    }, []);

    /*async function remove() {
        await deleteArticle(id);
        router.push('/');
    }*/

    async function remove(articleId: number) {
        await deleteArticle(articleId);
        router.push('/');
      }

    return (

        <>

            {articles?.map((linearticle) =>
                <tr key={linearticle.id}>
                    <td><img className="imagelinearticle" src={linearticle.image} alt={linearticle.name} /></td>
                    <td>{linearticle.id}</td>
                    <td>{linearticle.name}</td>
                    <td>{linearticle.artist.firstName}</td>
                    <td>{linearticle.description}</td>
                    <td>
                    <button onClick={() => linearticle.id ? remove(linearticle.id) : undefined}>
                        <FaTrashAlt/>
                    </button>
                </td>
                <td>
                <Link href="/user/edit/[id]" as={`/user/edit/${linearticle.id}`}><button>
                        <FaEdit/>
                    </button></Link>
                </td>
                </tr>
            )}

        </>

    );

}
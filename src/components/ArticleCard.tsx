import { Article, Stock } from "@/entities";
import { fetchOneArticleStock } from "@/stock-service";
import Link from "next/link";
import { useEffect, useState } from "react";
import { Card } from "react-bootstrap";

interface Props{
    article: Article;
}

export default function ArticleCard({article}:Props){

const [stocks, setStocks] = useState<Stock[]>();

    useEffect(() => {
        fetchOneArticleStock(Number(article.id))
            .then(data => setStocks(data))
    }, []);

        return(
        <>
        <Link className="no-style" href={"/article/"+article.id}>
            <Card className="article-card h-100">
                <div className="p-2 p-md-3 p-lg-4 article-card-grey">
                    <div className="article-img-container">
                        <img src={article.image} className="square-article-img"/>
                    </div>
                </div>
                <Card.Body  className="article-card-grey">
                <Card.Text className="d-flex flex-column justify-content-center align-items-center">
                    <span>{article.name}</span>  
                </Card.Text>
                <div className="d-flex flex-column justify-content-center align-items-center">
                    <hr className="little-hr"/>
                </div>
                <Card.Text className="d-flex flex-column justify-content-center align-items-center">
                    <>A partir de {stocks && stocks[0].price} €</>
                </Card.Text>
                </Card.Body>
            </Card>
        </Link>
        </>
    )
}
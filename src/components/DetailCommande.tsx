import { AuthContext } from "@/auth/auth-context";
import { Order, User } from "@/entities"
import { fetchCurrentOrder } from "@/order-service";
import { fetchCurrentUser, updateUser } from "@/user-service"
import { useContext, useEffect, useState } from "react"
import { Button, Form, Modal } from "react-bootstrap";
import { useForm } from "react-hook-form";

interface Props {
    order: Order
}

export default function DetailCommande({ order }: Props) {

    const [user, setUser] = useState<User>()
    const { token, setToken } = useContext(AuthContext);

    const [show, setShow] = useState(false);

    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);

    const { register, handleSubmit, formState: { errors } } = useForm<User>();


    useEffect(() => {
        fetchCurrentUser()
            .then(data => setUser(data))
            .catch(error => console.log(error))

    }, [])

    const onSubmit = async (data: User) => {
        console.log(data);
        setUser(data)
        updateUser(data, Number(user?.id))
          setShow(false);
    };

    return (
        <>
            {token &&
                <div className="col-md-12 d-flex justify-content-center mt-5 mb-5">
                    <div className="card mb-3" style={{ width: "630px" }}>
                        <div className="row g-0 bgcolor">

                            <div className="col-md-8">
                                <div className="card-body mt-1">
                                    <h5 className="card-title">Détails</h5>
                                    <hr />
                                    <div className=" mt-3 mb-3">

                                        <p>Utilisateur : {user?.firstName} {user?.lastName}</p>
                                        <p>N° Téléphone : <span>{user?.phoneNumber}</span></p>
                                        <p>Email : <span>{user?.email}</span></p>
                                        <p>Adresse : <span>{user?.street} {user?.ZIPCode} {user?.city}</span></p>
                                        {order &&
                                            <>
                                                <p>Date : <span>{order.date}</span></p>
                                                <p>Estimated Delivery time  : <span>{order.deliveryTime}</span></p>
                                                <p>Status : <span>{order.status}</span></p>
                                            </>
                                        }

                                        <Button className="mt-3 me-5" variant="primary" onClick={handleShow}>
                                            Edit
                                        </Button>
                                        <Button className="mt-3" variant="primary" onClick={() => alert("Non, tu ne supprimeras pas ton panier")}>
                                            Delete
                                        </Button>
                                    </div>

                                    <Modal show={show} onHide={handleClose}>
                                        <Modal.Body>

                                            <Form onSubmit={handleSubmit(onSubmit)}>

                                                <h2>Modifier vos infos pour la livraison</h2>

                                                <Form.Group controlId="formBasicname">
                                                    <Form.Label>nom :</Form.Label>
                                                    <Form.Control type="name" defaultValue={user?.firstName} {...register("firstName")} />
                                                    <Form.Label>prenom :</Form.Label>
                                                    <Form.Control type="name" defaultValue={user?.lastName} {...register("lastName")} />
                                                </Form.Group>

                                                <Form.Group controlId="formBasicphonen">
                                                    <Form.Label>Adresse :</Form.Label>
                                                    <Form.Control type="phonen" defaultValue={user?.phoneNumber} {...register("phoneNumber")} />
                                                </Form.Group>

                                                <Form.Group controlId="formBasicemail">
                                                    <Form.Label>Adresse :</Form.Label>
                                                    <Form.Control type="email" defaultValue={user?.email} {...register("email")} />
                                                </Form.Group>

                                                <Form.Group controlId="formBasicstreet">
                                                    <Form.Label>Adresse :</Form.Label>
                                                    <Form.Control type="street" defaultValue={user?.street} {...register("street")} />
                                                </Form.Group>

                                                <Form.Group controlId="formBasiccity">
                                                    <Form.Label>Ville :</Form.Label>
                                                    <Form.Control type="city" defaultValue={user?.city} {...register("city")} />
                                                </Form.Group>

                                                <Form.Group controlId="formBasiczip">
                                                    <Form.Label>Code Postal :</Form.Label>
                                                    <Form.Control type="zip" defaultValue={user?.ZIPCode} {...register("ZIPCode")} />
                                                </Form.Group>

                                                <Button className="mt-3" variant="primary" type="submit">
                                                    Update
                                                </Button>
                                            </Form>
                                        </Modal.Body>
                                    </Modal>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            }
        </>
    )
}
import { fetchAllArtist } from "@/article-service";
import { Artist, Article} from "@/entities";
import { useRouter } from "next/router";
import { useEffect, useState, FormEvent } from "react";

interface Props {
    onSubmit: (article: Article) => void;
    edited?: Article;
  }

export default function FormEditArticle({ onSubmit, edited }: Props) {

    const [errors, setErrors] = useState("");
    const router = useRouter();
    const { id } = router.query;
    const [artist, setArtist] = useState<Artist[]>([]);
    const [article, setArticle] = useState<Article>(edited?edited:{
        name: "",
        description: "",
        image: "",
        artist:  { id: 1},
      });

      /*----------------------------- */

    useEffect(() => {
        if (!id) {
            return;
        }
        fetchAllArtist()
            .then(data => setArtist(data))
            .catch(error => {
                console.log(error);
                if (error.response.status == 404) {
                    router.push('/404');
                }
            });

    }, [id]);

    /*----------------------------- */

    /*function handleChange(event: any) {
        setArticle({
          ...article,
          [event.target.name]: event.target.value,
        });
      }*/

      async function handleSubmit(event: FormEvent) {
        event.preventDefault();
        try {
          onSubmit(article);
        } catch (error: any) {
          if (error.response.status === 400) {
            setErrors(error.response.data.detail);
          }
        }
      }

    return (
        <>
            <form onSubmit={handleSubmit}>
                {errors && <p>{errors}</p>}
                <div>
                    <div>
                        <a>Form</a>
                        <div>
                            <input
                                type="text"
                                name="title"
                                value={article.name}
                                onChange={(e) => setArticle({ ...article, name: e.target.value })}
                                placeholder="Inserisci il titolo"
                                required
                            />
                        </div>
                        <div>
                            <input
                                type="text"
                                name="description"
                                value={article.description}
                                placeholder="Inserisci la descrizione"
                                onChange={(e) => setArticle({ ...article, description: e.target.value })}
                                required
                            />
                        </div>
                        <div>
                            <input
                                type="text"
                                name="image"
                                value={article.image}
                                placeholder="Inserisci link dell'immagine"
                                onChange={(e) => setArticle({ ...article, image: e.target.value })}
                                required
                            />
                        </div>
                        <div>
                            <select>
                            <option value="">Selectionner un artist</option>
                                {artist.map((option) => (
                                      <option key={option.id} value={option.id}>{option.firstName}</option>
                                ))}
                            </select>
                        </div>
                        <button>Submit</button>

                    </div>
                </div>

            </form>
        </>

    )

}
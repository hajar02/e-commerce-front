import { useEffect, useState } from "react";
import { fetchLatestArticles } from "@/article-service";
import { useRouter } from "next/router";
import { Article } from "@/entities";
import React from 'react';
import Link from "next/link";

export default function Nouveautes() {
  const [articles, setArticles] = useState<Article[]>([]);
  const router = useRouter();

  useEffect(() => {
    fetchLatestArticles()
      .then(data => setArticles(data))
      .catch(error => {
        console.log(error);
        if (error.response?.status == 404) {
     router.push('/404');
        }
      });
  }, []);

    return (
        <>
            <section className="nouveautes">
                <h2>NOUVEAUTES</h2>
                <hr />
                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Perferendis, ut doloribus, laudantium delectus cumque incidunt</p>
                <div className="container-nouveautes">
                {articles.map((article) => 
            <Link href={"/article/"+article.id}><div className="nouveau-article" key={article.id}>
              <img src={article.image} alt={article.name} />
              <div>{article.name}</div>
             </div>
             </Link>
             
          )}
                </div>
                <button><Link href={"/article"}>voir plus</Link></button>
            </section>
        </>
    )
}
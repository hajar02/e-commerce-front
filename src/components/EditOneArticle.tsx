
import { updateArticle } from "@/article-service";
import { Article } from "@/entities";
import { useRouter } from "next/router";
import Form from "./Form";
import { useState } from "react";

export default function EditOneArticle() {
    const router = useRouter();
    const [article, setArticle] = useState<Article>();

    async function update(article: Article) {
        const updated = await updateArticle(article);
        setArticle(updated);
    }

    
    return (
        <>
            <h1>Edit Article</h1>
            <Form edited={article} onSubmit={update} />
        </>
    );
}
export default function NosProduits(){
    return (
        <section className="container-fluid px-2 px-md-5">
            <div className="row g-3">
                <div className="col-md-6 p-5">
                    <div className="p-2 p-md-5">
                        <div className="px-2 px-md-5">
                            <div className="px-2 px-md-5">
                                <div className="image">
                                    <img src="https://images.unsplash.com/photo-1472289065668-ce650ac443d2?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1169&q=80" className="img img-responsive full-width img-product"/>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="col-md-6">
                    <div className="d-flex flex-column justify-content-center h-100">
                        
                            <h2>Nos produits</h2>
                            <h3>Lorem ipsum dolor sit amet consectetur</h3>
                            <hr className="underline"/>
                            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Dolores, molestias voluptate aut sint facilis perspiciatis dicta magnam laborum tempora animi ut adipisci, dignissimos accusamus minus minima quae nam cupiditate earum.</p>
                        
                    </div>
                </div>
            </div>
        </section>
        
    )
}
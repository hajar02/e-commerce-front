import { Article, LineArticles, Order, Stock } from "@/entities";
import { deleteLineArticle, updateLineArticle } from "@/order-service";
import { fetchAllSizeFromOneArticle } from "@/stock-service";
import { useRouter } from "next/router";
import { useState } from "react"

interface Props {
    order: Order
    onQuantityChange: () => void
}

export default function ArticleCommande({ order, onQuantityChange }: Props) {

    const router = useRouter();
    const [size, setSize] = useState<Article[]>();

    async function increment(lineArticle: LineArticles) {
        lineArticle.quantity++;
        await updateLineArticle(lineArticle)
        onQuantityChange()
    }

    async function decrement(lineArticle: LineArticles) {
        if (lineArticle.quantity > 1) {
            lineArticle.quantity--;
            await updateLineArticle(lineArticle)
            onQuantityChange()
        }
    }

    async function ELIMINARE(lineArticle: LineArticles) {
        await deleteLineArticle(lineArticle)
        router.reload();
    }

    async function getAllSize(lineArticle: LineArticles) {
        const response = await fetchAllSizeFromOneArticle(lineArticle.articleId)
        setSize(response)
        return response
    }

    async function updateSize(lineArticle: LineArticles, stock: Stock) {
        console.log(lineArticle, stock);
        await updateLineArticle({ ...lineArticle, stock: stock.id })
        onQuantityChange()
    }



    return (
        <>
            {order.lineArticles && order.lineArticles.map((lineArticle) =>
                <div key={lineArticle.id} className="col-md-12 d-flex justify-content-center mt-5 mb-5">
                    <div className="card mb-3" style={{ width: "630px" }}>
                        <div className="row g-0 bgcolor">

                            <div className="col-md-4 d-flex justify-content-center align-items-center">
                                <img style={{ height: "75%" }} src={lineArticle.productImage} className="img-fluid rounded" alt="..." />
                            </div>

                            <div className="col-md-8 d-flex">
                                <div className="card-body mt-1 d-flex flex-column justify-content-center ">
                                    <h5 className="card-title">{lineArticle.productName}</h5>
                                    <hr />
                                    <div className="panierDetail mt-3 mb-3">

                                        <p>{lineArticle.productPrice} €</p>

                                        <div className="dropdown btnSmall">
                                            <button onClick={() => getAllSize(lineArticle)} className="dropdown-toggle" type="button" data-bs-toggle="dropdown" aria-expanded="false">
                                                {lineArticle.productSize}
                                            </button>
                                            <ul className="dropdown-menu">
                                                <p>Select a new Size :</p>
                                                {size && size.map((item: Stock) =>
                                                    <>
                                                        <li><button onClick={() => updateSize(lineArticle, item)}>{item.size}</button></li>
                                                    </>
                                                )}
                                            </ul>
                                        </div>

                                        <button onClick={() => decrement(lineArticle)}>-</button>
                                        <span>{lineArticle.quantity}</span>
                                        <button onClick={() => increment(lineArticle)}>+</button>

                                    </div>
                                    <span className="ACremove card-text"><small className="text-muted" onClick={() => ELIMINARE(lineArticle)}>Remove</small></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            )}
        </>
    )
}

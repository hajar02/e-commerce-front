


export default function QuiSommeNous() {


    return (
        <>
            <div className="container-fluid">
                <div className="row">
                    <div className="col-md-6" style={{ backgroundColor: "lightGrey" }}>
                        <div className="d-flex flex-column justify-content-center h-100 p-2 p-md-5">
                            <h2 className="QSNitem  QSNtitle">Qui Somme Nous ?</h2>
                            <h3 className="QSNitem ">Lorem ipsum dolor sit amet, consectetur</h3>
                            <hr className="QSNitem  QSNhr" />
                            <p className="QSNitem ">
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
                            </p>
                        </div>
                    </div>
                    <div className="col-md-6 QSNimg">
                        <img className="img-fluid" src="https://img.passeportsante.net/1200x675/2022-11-14/welsh-corgi.webp" alt="" />
                    </div>
                </div>
            </div>
        </>
    )
}
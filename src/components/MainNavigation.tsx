import { AuthContext } from '@/auth/auth-context';
import { useContext } from 'react';
import Container from 'react-bootstrap/Container';
import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';
import NavDropdown from 'react-bootstrap/NavDropdown';
import { FaUser, FaShoppingCart } from 'react-icons/fa';

export default function MainNavigation() {

  const { token, setToken } = useContext(AuthContext);

  function logOut(){
    setToken(null)
    window.location.reload();
  }

  return (
    <Navbar bg="light" expand="lg">
      <Container>
        <Navbar.Brand href="/">LOGO</Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
          <Nav className="me-auto">
            <Nav.Link href="/">Home</Nav.Link>
            <Nav.Link href="/article">Articles</Nav.Link>
            {/*//Pour le moment on a pas besoin du dropdown donc je l'enlève 
            <NavDropdown title="Boh" id="basic-nav-dropdown">
              <NavDropdown.Item href="#action/3.1">Action</NavDropdown.Item>
              <NavDropdown.Item href="#action/3.2">
                Another action
              </NavDropdown.Item>
              <NavDropdown.Item href="#action/3.3">Something</NavDropdown.Item>
              <NavDropdown.Divider />
              <NavDropdown.Item href="#action/3.4">
                Separated link
              </NavDropdown.Item>
            </NavDropdown> */}
          </Nav>
          <Nav className="ml-auto">
            <Nav.Link href="/panier"><FaShoppingCart /></Nav.Link>
            <NavDropdown title={<FaUser />} id="account-dropdown">
              <NavDropdown.Item href="#action/4.1">Profile</NavDropdown.Item>
              <NavDropdown.Item onClick={() => console.log(token)}>
                Settings
              </NavDropdown.Item>
              <NavDropdown.Divider />
              {token ?
              <NavDropdown.Item onClick={() => logOut()}>Logout</NavDropdown.Item>
              :
                <NavDropdown.Item href="/login" >Login</NavDropdown.Item>
              }
              <NavDropdown.Divider />
              <NavDropdown.Item href="/signIn" >Sign In</NavDropdown.Item>
            </NavDropdown>
          </Nav>
        </Navbar.Collapse>
      </Container>
    </Navbar>
  );
}

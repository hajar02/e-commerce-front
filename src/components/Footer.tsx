import { FaFacebook, FaSnapchat, FaInstagram, FaLinkedin } from 'react-icons/fa';

export default function Footer() {


    return (
        <>
            <section className="footerr">
                <a href="#">
                    <img src="https://cryptologos.cc/logos/uniswap-uni-logo.png" alt="logo footer" />
                </a>

                <div className="social-icons-container">
                <a href="https://www.facebook.com/" className="social-icon" target="_blank" ><FaFacebook /></a>
                <a href="https://www.snapchat.com/" className="social-icon" target="_blank" ><FaSnapchat /></a>
                <a href="https://www.instagram.com/" className="social-icon" target="_blank" ><FaInstagram /></a>
                <a href="https://www.linkedin.com/" className="social-icon" target="_blank" ><FaLinkedin /></a>
                </div>

                <div>Web site created by Alexandre, Juliette and Hajar</div>

            </section>
            
        </>
    )

}        
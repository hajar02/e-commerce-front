import FormAddArticle from "@/components/FormAddArticle";
import { postArticle } from "@/article-service";
import { Article, Artist } from "@/entities";
import { useRouter } from "next/router";
import Form from '@/components/Form'

export default function AddOneArticle() {
    const router = useRouter();

    async function addArticle(article: Article, artist: Artist) {
        const added = await postArticle(article, artist);
        router.push('/article/' + added.id);
    }

    return (
    <>
        <h1>Add Article</h1>
            <Form onSubmit={addArticle} />
        </>
    );
}
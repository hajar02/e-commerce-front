import { fetchAllArtist } from "@/article-service";
import { Article, Artist } from "@/entities"
import { useRouter } from "next/router";
import { FormEvent, useEffect, useState } from "react"
import { Dropdown } from "react-bootstrap";

interface Props {
    onSubmit:(article:Article, artist:Artist) => void;
    edited?:Article;
}

export default function FormArticle({onSubmit, edited}:Props) {

    const [errors, setErrors] = useState("");
    const router = useRouter();
    const { id } = router.query;
    const [artist, setArtist] = useState<Artist[]>([]);
    const [selectedArtist, setSelectedArtist] = useState<Artist>();
    const [article, setArticle] = useState<Article>({
        name: "",
        description: "",
        image: "",
        artist:  { id: 1},
    });

    console.log("test Article "+article.name)

    useEffect(() => {
        if (edited) {
          setArticle(edited);
        }
      }, [edited]);

    console.log("edited "+edited);

    function handleChange(event: any) {
        setArticle({
            ...article,
            [event.target.name]:event.target.value
        });
        console.log("setArticle "+article)
    }

    async function handleSubmit(event:FormEvent) {
        event.preventDefault();
        if(selectedArtist){
           try {
            onSubmit(article, selectedArtist);

            } catch(error:any) {
                if(error.response.status == 400) {
                    setErrors(error.response.data.detail);
                }
            } 
        } else {
            alert("You must select an artist")
        }
        

    }

    /*----------------------------- */

    useEffect(() => {
        
        fetchAllArtist()
            .then(data => setArtist(data))
            .catch(error => {
                console.log(error);
                if (error.response.status == 404) {
                    router.push('/404');
                }
            });

    }, [id]);

    /*----------------------------- */


    return (

        <>
            <form onSubmit={handleSubmit}>
                {errors && <p>{errors}</p>}
                <div>
                    <div>
                        <a>Form</a>
                        <div>
                            <input
                                type="text"
                                name="name"
                                value={article.name}
                                placeholder="Inserisci il titolo"
                                onChange={handleChange}
                                required
                            />
                        </div>
                        <div>
                            <input
                                type="text"
                                name="description"
                                value={article.description}
                                placeholder="Inserisci la descrizione"
                                onChange={handleChange}
                                required
                            />
                        </div>
                        <div>
                            <input
                                type="text"
                                name="image"
                                value={article.image}
                                placeholder="Inserisci link dell'immagine"
                                onChange={handleChange}
                                required
                            />
                        </div>
                        
                        <Dropdown>
                        <Dropdown.Toggle variant="success" id="dropdown-basic">
                            {selectedArtist ?  selectedArtist.firstName : "Select an artist"}
                        </Dropdown.Toggle>

                        <Dropdown.Menu>
                        {artist.map((item) =>
                            <Dropdown.Item key={item.id} as="button" onClick={() => setSelectedArtist(item)} value={item.firstName}>{item.firstName}</Dropdown.Item>
                        )}
                        </Dropdown.Menu>
                    </Dropdown>

                        <button>Submit</button>

                    </div>
                </div>

            </form>
        </>
    )
}


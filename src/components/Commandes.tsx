import Table from 'react-bootstrap/Table';
import AddOneArticle from './AddOneArticle';
import LineArticleTable from './LineArticleTable';

export default function Commandes() {

  /*function toggle() {
    setShowEdit(!showEdit);
  }*/

  return (
    <>
      <section className='commandes'>
        <h2>COMMANDES</h2>
        <Table striped bordered hover size="sm" className='tablecommandes'>
          <thead>
            <tr>
              <th>Image</th>
              <th>N°</th>
              <th>Titre</th>
              <th>Artiste</th>
              <th>Description</th>
            </tr>
          </thead>
          <tbody>
            <LineArticleTable />
          </tbody>
        </Table>
        <AddOneArticle />
      </section>

      {/*{showEdit &&
        <>
          <FormArticle onSubmit={addArticle} />
        </>
      }*/}

    </>
  );
}
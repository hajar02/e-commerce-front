import axios from "axios";
import { LineArticles, Order, Stock } from "./entities";



export async function postLineArticle(lineArticle:LineArticles,order:Order,stock:Stock){
    const response = await axios.post<LineArticles>('/api/lineArticle/'+order.id+"/"+stock.id, lineArticle);
    return response.data;
}

import axios from "axios";
import { Article, Artist, Comment, Stock } from "./entities";


export async function fetchAllArticle() {
    const response = await axios.get<Article[]>('/api/article');
    return response.data;
}

export async function fetchOneArticle(id: number) {
    const response = await axios.get<Article>('/api/article/' + id);
    return response.data;
}

export async function fetchLatestArticles() {
    const response = await axios.get<Article[]>('api/article/latest_articles');
    return response.data;
}

export async function fetchOneStock(id: number) {
    const response = await axios.get<Stock>('/api/stock/' + id);
    return response.data;
}

export async function fetchAllComments(article_id: number) {
    const response = await axios.get<Comment[]>(`/api/comment/`+ article_id);
    return response.data;
}

export async function fetchAllArtist() {
    const response = await axios.get<Artist[]>('/api/artist');
    return response.data;
}

export async function postArticle(article:Article, artist:Artist) {
    const response = await axios.post<Article>('/api/article/'+artist.id, article);
    return response.data;
}

export async function deleteArticle(id:any) {
    await axios.delete('/api/article/'+id);
}

export async function updateArticle(article:Article) {
    const response = await axios.put<Article>('api/article/'+article.id, article);
    return response.data;
}
 
export async function fetchAllArticleByArtist(artistName : string){
    const response = await axios.get<Article[]>('/api/article/articlesByArtist/'+artistName);
    return response.data
}

export async function commentsByArticle(articleId: number) {
    const response = await axios.get<Comment[]>('api/comment/article/'+ articleId);
    return response.data || [];
}

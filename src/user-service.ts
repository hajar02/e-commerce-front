import axios from "axios";
import { User } from "./entities";



export async function fetchCurrentUser(){
    const response = await axios.get<User>('/api/user/current');
    return response.data;
}

export async function updateUser(user:User,id:number){
    const response = await axios.put<User>('/api/user/'+id, user);
    return response.data;
}
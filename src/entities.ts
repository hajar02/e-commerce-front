export interface User {
    id?:number;
    firstName?: string;
    lastName?: string;
    email?: string;
    hashedPassword?: string;
    phoneNumber?: string;
    street?: string;
    ZIPCode?: string;
    city?: string;
    role?: string;
}


export interface Article {
    id?: number;
    name: string;
    description: string;
    image: string;
    averageStar?: number;
    artist: Artist;
    stocks?: Stock;
}

export interface Stock {
    // map(arg0: (ffs: Stock) => JSX.Element): import("react").ReactNode;
    id?: number;
    article_id?: number;
    size?: string;
    price?: number;
    quantity?:number;
}



export interface Order {
    id?:number;
    user_id?:number;
    date:string;
    total:number;
    deliveryTime:string;
    status:string;
    lineArticles?:LineArticles[];
}

export interface Stock{
    id?: number;
    article_id?: number;
    size?: string;
    price?: number;
    quantity?:number;
}

export interface Comment{
    id?: number;
    article_id?: number;
    text?: string;
}

export interface LineArticles {
    articleId:number;
    id?: number;
    price? : number;
    productName?: string;
    productImage?: string;
    productPrice?: number;
    productSize?: string;
    quantity : number ;
    star : number;
    stock?:number
}

export interface Artist{
    id?: number;
    firstName?: string;
    lastName?: string;
    description?: string;
    image?: string;
}